+--------+-----------+---------------------------+--------------------+---------------------------------------------------+------------+
| Domain | Method    | URI                       | Name               | Action                                            | Middleware |
+--------+-----------+---------------------------+--------------------+---------------------------------------------------+------------+
|        | GET|HEAD  | /                         |                    | Closure                                           | web        |
|        | POST      | api/authors               | authors.store      | App\Http\Controllers\AuthorsController@store      | api        |
|        | GET|HEAD  | api/authors               | authors.index      | App\Http\Controllers\AuthorsController@index      | api        |
|        | PUT|PATCH | api/authors/{author}      | authors.update     | App\Http\Controllers\AuthorsController@update     | api        |
|        | DELETE    | api/authors/{author}      | authors.destroy    | App\Http\Controllers\AuthorsController@destroy    | api        |
|        | GET|HEAD  | api/authors/{author}      | authors.show       | App\Http\Controllers\AuthorsController@show       | api        |
|        | GET|HEAD  | api/books                 | books.index        | App\Http\Controllers\BooksController@index        | api        |
|        | POST      | api/books                 | books.store        | App\Http\Controllers\BooksController@store        | api        |
|        | GET|HEAD  | api/books/{book}          | books.show         | App\Http\Controllers\BooksController@show         | api        |
|        | PUT|PATCH | api/books/{book}          | books.update       | App\Http\Controllers\BooksController@update       | api        |
|        | DELETE    | api/books/{book}          | books.destroy      | App\Http\Controllers\BooksController@destroy      | api        |
|        | GET|HEAD  | api/categories            | categories.index   | App\Http\Controllers\CategoriesController@index   | api        |
|        | POST      | api/categories            | categories.store   | App\Http\Controllers\CategoriesController@store   | api        |
|        | GET|HEAD  | api/categories/{category} | categories.show    | App\Http\Controllers\CategoriesController@show    | api        |
|        | PUT|PATCH | api/categories/{category} | categories.update  | App\Http\Controllers\CategoriesController@update  | api        |
|        | DELETE    | api/categories/{category} | categories.destroy | App\Http\Controllers\CategoriesController@destroy | api        |
|        | GET|HEAD  | api/loanBooks             | loanBooks.index    | App\Http\Controllers\LoanBooksController@index    | api        |
|        | POST      | api/loanBooks             | loanBooks.store    | App\Http\Controllers\LoanBooksController@store    | api        |
|        | PUT|PATCH | api/loanBooks/{loanBook}  | loanBooks.update   | App\Http\Controllers\LoanBooksController@update   | api        |
|        | DELETE    | api/loanBooks/{loanBook}  | loanBooks.destroy  | App\Http\Controllers\LoanBooksController@destroy  | api        |
|        | GET|HEAD  | api/loanBooks/{loanBook}  | loanBooks.show     | App\Http\Controllers\LoanBooksController@show     | api        |
|        | GET|HEAD  | api/settings              | settings.index     | App\Http\Controllers\SettingsController@index     | api        |
|        | POST      | api/settings              | settings.store     | App\Http\Controllers\SettingsController@store     | api        |
|        | GET|HEAD  | api/settings/{setting}    | settings.show      | App\Http\Controllers\SettingsController@show      | api        |
|        | PUT|PATCH | api/settings/{setting}    | settings.update    | App\Http\Controllers\SettingsController@update    | api        |
|        | DELETE    | api/settings/{setting}    | settings.destroy   | App\Http\Controllers\SettingsController@destroy   | api        |
|        | GET|HEAD  | api/users                 | users.index        | App\Http\Controllers\UsersController@index        | api        |
|        | POST      | api/users                 | users.store        | App\Http\Controllers\UsersController@store        | api        |
|        | GET|HEAD  | api/users/{user}          | users.show         | App\Http\Controllers\UsersController@show         | api        |
|        | PUT|PATCH | api/users/{user}          | users.update       | App\Http\Controllers\UsersController@update       | api        |
|        | DELETE    | api/users/{user}          | users.destroy      | App\Http\Controllers\UsersController@destroy      | api        |
+--------+-----------+---------------------------+--------------------+---------------------------------------------------+------------+
