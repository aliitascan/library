<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::apiResource('/authors', 'AuthorsController');
Route::apiResource('/books', 'BooksController');
Route::apiResource('/categories', 'CategoriesController');
Route::apiResource('/loanBooks', 'LoanBooksController');
Route::apiResource('/settings' , 'SettingsController');
Route::apiResource('/users', 'UsersController');
Route::get('/test', 'AuthorsController@test');
