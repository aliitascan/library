<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanBooks extends Model
{
    protected $fillable = [
        'given_date',
        'date_of_return',
        'returned_date',
        'delay_fine',
        'user_id',
        'isbn',
    ];
    public function loan_books()
    {
        return $this->hasMany(LoanBooks::class);
    }
}
