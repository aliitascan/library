<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $fillable=[
        'isbn',
        'book_title',
        'published_date',
        'author_id',
        'category_id',
    ];
}
