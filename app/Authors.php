<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    protected $fillable =[
        'author_name',
        'author_surname',
    ];
    public function books()
    {
        return $this->hasMany(Books::class);
    }
}
