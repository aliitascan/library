<?php

namespace App\Http\Controllers;

use App\LoanBooks;
use App\Http\Resources\LoanBooksResource;
use App\Http\Resources\LoanBooksResourceCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanBooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanBooks  $loanBooks
     * @return \Illuminate\Http\Response
     */
    public function show(LoanBooks $loanBooks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanBooks  $loanBooks
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanBooks $loanBooks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanBooks  $loanBooks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanBooks $loanBooks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanBooks  $loanBooks
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanBooks $loanBooks)
    {
        //
    }
}
