<?php

namespace App\Http\Controllers;

use App\Authors;
use App\Http\Resources\AuthorsResource;
use App\Http\Resources\AuthorsResourceCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AuthorsResourceCollection
    {
        return new AuthorsResourceCollection(Authors::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'author_name' => 'required',
            'author_surname' => 'required',
        ]);
        $author = Authors::create($request->all());
        return new AuthorsResource($author);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Authors $authors
     * @return \Illuminate\Http\Response
     */
    public function show($id) : AuthorsResource
    {
        return new AuthorsResource(Authors::find($id));
    }
    public function test(){
        return json_encode("test");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Authors $authors
     * @return \Illuminate\Http\Response
     */
    public function edit(Authors $authors)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Authors $authors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Authors $authors)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Authors $authors
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authors $authors)
    {
        //
    }
}
