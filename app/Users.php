<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = [
        'user_name',
        'user_address',
        'phone_number',
        'email_address',
        'other_details',
    ];
    public function loan_books()
    {
        return $this->hasMany(LoanBooks::class);
    }
}
